import uuid
from datetime import datetime, timedelta
from settings import db
from peewee import *
from telegram import Emoji


class Chat(Model):
    chat_id = IntegerField()
    trigger_time = TimeField(null=True)
    last_trigger = DateTimeField(null=True)
    code = CharField(null=True)
    code_expire = DateTimeField(null=True)

    def get_code(self):
        self.code_expire = datetime.now() + timedelta(minutes=30)
        return str(uuid.uuid4().get_hex().upper()[0:6])

    def updates(self):
        return Update.select().join(Chat)

    def trigger(self, date_time):
        self.last_trigger = date_time
        self.save()

    def should_trigger(self):
        if self.trigger_time is None:
            return False

        now = datetime.now()

        # trigger time isn't set
        if self.last_trigger is None:

            if now.time() > self.trigger_time:
                self.trigger(now)

                return True

        # already triggered today
        if self.last_trigger is not None and now.date() == self.last_trigger.date():
            return False

        if now.time() > self.trigger_time:
            self.trigger(now)
            return True

        return False

    class Meta:
        database = db


class Update(Model):
    moods = [Emoji.PENSIVE_FACE,
             Emoji.UNAMUSED_FACE,
             Emoji.CONFUSED_FACE,
             Emoji.NEUTRAL_FACE,
             Emoji.SMILING_FACE_WITH_SMILING_EYES,
             Emoji.SMILING_FACE_WITH_OPEN_MOUTH]

    chat = ForeignKeyField(rel_model=Chat)
    mood = CharField()
    description = TextField(null=True)
    recorded_at = DateTimeField()

    def to_dict(self):
        return {
            'mood': self.mood,
            'recorded_at': self.recorded_at.__str__(),
            'description': self.description,
            'mood_score': self.moods.index(self.mood.encode('utf-8'))
        }

    class Meta:
        database = db
