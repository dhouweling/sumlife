from settings_local import BOT_KEY
from sumlife.bot import Bot
from sumlife.commands import Log, SetTriggerTime, Summary, Show
from sumlife.triggers import DateTime

if "__main__" == __name__:

    commands = {
        '/log': Log,
        '/set_time': SetTriggerTime,
        '/summary': Summary,
        '/show': Show,
    }

    bot = Bot(commands=commands, api_key=BOT_KEY)
    bot.add_trigger(DateTime(command=Log))
    bot.run()
