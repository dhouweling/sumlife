import abc


class Trigger:
    __metaclass__ = abc.ABCMeta

    def __init__(self, command):
        self.command = command

    @abc.abstractmethod
    def applies(self):
        pass

    def set_chat(self, chat):
        self._chat = chat


class DateTime(Trigger):
    def applies(self):
        if self._chat.should_trigger():
            return True

        return False
