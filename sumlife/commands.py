import abc
import datetime

import telegram

from models import Update


class BaseCommand(object):
    __metaclass__ = abc.ABCMeta

    def __init__(self, chat, telegram_client):
        self._telegram_client = telegram_client
        self.complete = False
        self.chat = chat
        self.stage = 0
        self.command_stack = []
        self.command_stack.append(self.start_command)

    def is_complete(self):
        return self.complete

    def set_complete(self):
        self.command_complete()
        self.complete = True

    def before_execute(self):
        pass

    def after_execute(self):
        pass

    def command_complete(self):
        pass

    def start_command(self, message):
        pass

    def execute(self, message):
        self.before_execute()

        if len(self.command_stack) == 0:
            self.set_complete()
            return

        command = self.command_stack.pop()
        command(message)
        self.after_execute()


class Cancel(BaseCommand):
    def __init__(self):
        super(Cancel, self).__init__()


class Summary(BaseCommand):
    def start_command(self, message):
        smiles = [update.mood for update in self.chat.updates()]

        smiles_str = "".join(smiles).encode('utf-8')
        if len(smiles_str) == 0:
            return

        self._telegram_client.sendMessage(self.chat.chat_id, smiles_str)


class SetTriggerTime(BaseCommand):
    def __init__(self, chat, telegram_client):
        super(SetTriggerTime, self).__init__(chat, telegram_client)

    def process_time(self, message):
        message = message.text
        try:
            time = datetime.datetime.strptime(message, "%H:%M")

            self.chat.trigger_time = time
            self.chat.save()

            self._telegram_client.sendMessage(chat_id=self.chat.chat_id,
                                              text='Registered your time')
        except ValueError:
            self._telegram_client.sendMessage(chat_id=self.chat.chat_id,
                                              text='The given format is incorrect :( use HH:MM format')
            self.command_stack.append(self.process_time)

        self.set_complete()

    def start_command(self, message):
        self._telegram_client.sendMessage(chat_id=self.chat.chat_id,
                                          text='When do you want me to ask for your summary')

        self._telegram_client.sendMessage(chat_id=self.chat.chat_id,
                                          text='Use 24 hour format')

        self.command_stack.append(self.process_time)


class Log(BaseCommand):
    def __init__(self, chat, telegram_client):
        super(Log, self).__init__(chat, telegram_client)

        self.update = Update(chat=chat, recorded_at=datetime.datetime.now())

        self.allowed_moods = [telegram.Emoji.PENSIVE_FACE,
                              telegram.Emoji.UNAMUSED_FACE,
                              telegram.Emoji.CONFUSED_FACE,
                              telegram.Emoji.NEUTRAL_FACE,
                              telegram.Emoji.SMILING_FACE_WITH_SMILING_EYES,
                              telegram.Emoji.SMILING_FACE_WITH_OPEN_MOUTH]

    def command_complete(self):
        self.update.save()

    def say_complete(self, message):
        self._telegram_client.sendMessage(chat_id=self.chat.chat_id,
                                          text="Okay done!")

    def process_description(self, message):
        self.update.description = message.text

        self._telegram_client.sendMessage(chat_id=self.chat.chat_id,
                                          text='Added description',
                                          reply_markup=telegram.ReplyKeyboardHide())
        self.set_complete()

    def process_ask_description(self, message):
        if message.text == "Yes":
            self._telegram_client.sendMessage(chat_id=self.chat.chat_id,
                                              text='Okay just type what you want to store',
                                              reply_markup=telegram.ReplyKeyboardHide())

            self.command_stack.append(self.process_description)
            return

        self._telegram_client.sendMessage(chat_id=self.chat.chat_id,
                                          text='No problem!',
                                          reply_markup=telegram.ReplyKeyboardHide())

        self.set_complete()

    def process_mood(self, message):
        self.update.mood = message.text
        self._telegram_client.sendMessage(chat_id=self.chat.chat_id,
                                          text='Want to give a summary?',
                                          reply_markup=telegram.ReplyKeyboardMarkup([["Yes", "No"]]))

        self.command_stack.append(self.process_ask_description)

    def start_command(self, message):
        self._telegram_client.sendMessage(chat_id=self.chat.chat_id,
                                          text='How do you feel today?',
                                          reply_markup=telegram.ReplyKeyboardMarkup([self.allowed_moods]))
        self.command_stack.append(self.process_mood)


class Show(BaseCommand):
    def start_command(self, message):
        # generate code
        code = self.chat.get_code()
        self._telegram_client.sendMessage(chat_id=self.chat.chat_id,
                                          text=code,
                                          reply_markup=telegram.ReplyKeyboardHide())
