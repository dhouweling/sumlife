from commands import *
from models import Chat


class Bot(object):
    DEBUG = True

    available_commands = {
        '/cancel': Cancel,
    }

    def __init__(self, api_key, commands=None):
        self._telegram_client = telegram.Bot(token=api_key)
        self.offset = 0
        self.current_command = None
        self._triggers = []

        c = self.available_commands.copy()
        c.update(commands)
        self.available_commands = c

    def add_trigger(self, trigger):
        self._triggers.append(trigger)

    def set_triggers(self, triggers):
        self._triggers += triggers

    def is_executing_command(self, chat):
        if self.current_command is None:
            return False

        try:
            return not self.current_command[chat.id].is_complete()
        except KeyError:
            return False

    def is_cancel_command(self, message):
        try:
            if self.available_commands[message] == cancel:
                return True

            return False
        except KeyError:
            return False

    def execute_current_command(self, message, chat):
        if not self.is_executing_command(chat):

            # Unset the current command
            self.current_command[chat.chat_id] = None

        else:
            # Run next stage of this command
            self.current_command[chat.id].execute(message)
            return

    def resolve_command(self, message, chat):
        # check if the update is a command
        if message.text in self.available_commands:
            self.set_command(chat, self.available_commands[message.text])

    def set_command(self, chat, command, message=None):
        self.current_command = {chat.id: command(chat, telegram_client=self._telegram_client)}
        self.execute_current_command(message, chat)

    def cancel_current_command(self, chat, message):
        pass
        # Cancel(chat).execute(message=message)
        # self.current_command[chat.chat_id] = None

    def process_updates(self):
        try:
            updates = self._telegram_client.getUpdates(offset=self.offset, limit=1, timeout=4)
        except Exception:
            return

        for update in updates:
            # Increment offset
            self.offset = update.update_id + 1

            # Create chat if not already created
            chat, created = Chat.get_or_create(chat_id=update.message.chat_id)

            message = update.message

            if self.is_cancel_command(update):
                self.cancel_current_command(chat)
                return

            # Check if there isn't a command already running
            if self.is_executing_command(chat) and self.execute_current_command(message, chat):
                # command isn't complete yet
                return

            self.resolve_command(update.message, chat)

    def execute_triggers(self, chat):
        # execute triggers when they are valid
        for trigger in self._triggers:
            trigger.set_chat(chat)
            if trigger.applies():
                self.set_command(chat=chat, command=trigger.command)

    def send_updates(self):
        for chat in Chat.select():

            if not self.is_executing_command(chat):
                self.execute_triggers(chat)

    def run(self):
        while True:
            self.send_updates()
            self.process_updates()
